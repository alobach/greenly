import 'package:flutter/material.dart';
import '../models/colors.dart' as colorModel;
import '../models/task.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key key}) : super(key: key);
  
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final num points = 105;
  List<Task> tasks;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: new Column(
            // mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
          Container(
              height: 400.0,
              color: new Color(0xFFffe6e6),
              child: new Column(
                children: <Widget>[
                  this.notificationCard(),
                  this.pointsWidget(context),
                  this.treeWidget(),
                  SizedBox(height: 20),
                  new Text(
                        'Thanks for caring about the earth!',
                        style: TextStyle(
                            color: colorModel.blackColor,
                            fontWeight: FontWeight.w400,
                            fontSize: 20.0),
                      )
                ],
              )),
          Expanded(child: new TasksListWidget()),
        ]));
  }

  Widget pointsWidget(BuildContext context) {
    var provider = Provider.of<TaskNotifier>(context);
    return new Container(
        margin: new EdgeInsets.only(top: 45.0),
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              elevation: 10.0,
              color: colorModel.dartGreen,
              child: new Container(
                padding:
                    new EdgeInsets.symmetric(vertical: 10.0, horizontal: 14.0),
                child: new Text(
                  provider.points().value.toString(),
                  style: TextStyle(color: Colors.white, fontSize: 20.0),
                ),
              ),
            ),
            new Text(
              'eco points',
              style: TextStyle(
                  color: colorModel.blackColor,
                  fontWeight: FontWeight.w400,
                  fontSize: 20.0),
            )
          ],
        ));
  }

  Widget treeWidget() {
    return new Container(
        margin: new EdgeInsets.only(top: 40.0),
        child: new ClipRRect(
            borderRadius: new BorderRadius.circular(50.0),
            child: new Container(
                padding:
                    new EdgeInsets.only(top: 20.0, right: 20.0, left: 20.0),
                color: Colors.white,
                child: new Column(
                  children: <Widget> [
                     new Image.asset('assets/tree_img.png')
                  ]))));
                 
  }

  Widget notificationCard() {
    return new Card(
        margin: new EdgeInsets.only(left: 15.0, right: 15.0, top: 30.0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        elevation: 10.0,
        child: new Padding(
          padding: new EdgeInsets.symmetric(vertical: 10.0, horizontal: 14.0),
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              new Text(
                'Did you know...?',
                style:
                    new TextStyle(fontWeight: FontWeight.bold, fontSize: 14.0),
              ),
              new Text(
                  "Lorem Ipsum is simply dummy text of the printing and typesetting industry. ",
                  style: new TextStyle(fontSize: 13.0)),
              new Text('Learn more',
                  style: new TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 12.0,
                      color: colorModel.jordyColor))
            ],
          ),
        ));
  }
}

class TasksListWidget extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<TaskNotifier>(context);

    return ListView(
        children:provider
            .tasks()
            .map((item) => new DismissButtonWidget(task: item))
            .toList());
  }
}

class DismissButtonWidget extends StatelessWidget {
  final Task task;

  const DismissButtonWidget({Key key, @required this.task}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dismissible(
      secondaryBackground: this.dismissBackground(),
      key: Key(task.id.toString()),
      onDismissed: (direction) {
        Provider.of<TaskNotifier>(context, listen: false).deleteTask(task.id);
      },
      background: Container(color: Colors.red),
      child: customListTile(task),
    );
  }

  Widget dismissBackground() {
    return new Container(
        color: colorModel.illusionColor,
        child: new Center(
          child: new Text("Doesn't apply ;)"),
        ));
  }

  Widget customListTile(Task item) {
    return Card(
      elevation: 3.0,
      child: ListTile(
        leading: new AddListButtonWidget(task: item),
        title: Text(item.title),
        subtitle: Text(item.desc),
      ),
    );
  }
}

class AddListButtonWidget extends StatelessWidget {
  final Task task;

  const AddListButtonWidget({Key key, @required this.task}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.favorite),
      color: task.active ? colorModel.dartGreen : colorModel.dartGreyColor,
      tooltip: 'Tap if finished ',
      onPressed: () => Provider.of<TaskNotifier>(context, listen: false)
          .changeState(task.id),
    );
  }
}
