import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../models/plans.dart';
import '../pages/personalizePage.dart';
import './homePage.dart';

class Item {
  Item({
    this.expandedValue,
    this.headerValue,
    this.isExpanded = false,
  });

  String expandedValue;
  String headerValue;
  bool isExpanded;
}

List<Item> generateItems(int numberOfItems) {
  return List.generate(numberOfItems, (int index) {
    return Item(
      headerValue: 'Panel $index',
      expandedValue: 'This is item number $index',
    );
  });
}

class PlansPage extends StatefulWidget {
  const PlansPage({Key key}) : super(key: key);
  
  @override
  _PlansPageState createState() => _PlansPageState();
}

/// This is the private State class that goes with MyStatefulWidget.
class _PlansPageState extends State<PlansPage> {
  List<Item> _data = [];

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      this.fetchPlans();
    });
  }

  void fetchPlans()
  {
    var provider = Provider.of<PlanNotifier>(context, listen: false);
    setState(() {
       var plans = provider.plans();
       plans.forEach((element) {
         this._data.add(new Item(
           headerValue: element.title,
           expandedValue: element.description));
       });
    });
  }


  @override
  Widget build(BuildContext context) {
     
    return SingleChildScrollView(
      child: Container(
        margin: new EdgeInsets.symmetric(
          vertical: 16.0,
          horizontal: 24.0),
        child: 
            new Column(
              children: this._data
                  .map((item) => GetStack(item))
                  .toList())
            
      ));
  }
  final planetThumbnail = new Container(
    margin: new EdgeInsets.symmetric(
      vertical: 40.0
    ),
    alignment: FractionalOffset.centerLeft,
    child: new Image.asset('assets/Basic.png', scale: 3.0,));

  Widget getContainerCard(Item item)
  {
    return new Container(
      height: 124.0,
      margin: new EdgeInsets.only(left: 55.0),
      child: ListTile(
            title: Text(item.headerValue),
            subtitle: Text(item.expandedValue),
            trailing: FlatButton(
                      onPressed: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) {
                              return FormPage();
                            },
                          ),
                        );
                      },
                      child: Text(
                        "Personilize",
                      ),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0),
                        side: BorderSide(color: Colors.red)
                      ),
                    )
            
      ),
      decoration: new BoxDecoration(
        color: new Color(0xFFffe6e6),
        shape: BoxShape.rectangle,
        borderRadius: new BorderRadius.circular(8.0),
        boxShadow: <BoxShadow>[
          new BoxShadow(  
            color: Colors.black12,
            blurRadius: 10.0,
            offset: new Offset(0.0, 10.0),
          ),
        ],
      ),
    );
  }

  Widget GetStack(Item item) {
    return new Stack(
      children: [
        getContainerCard(item),
        planetThumbnail
      ],
    );
  }
}