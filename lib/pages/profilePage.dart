import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import './loginPage.dart';
import '../sign_in.dart';
import '../models/task.dart';
import 'package:provider/provider.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key key}) : super(key: key);
  
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  final List<charts.Series> seriesList = _createSampleData();
  final animate = false;
  Plan currentPlan;
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      var provider = Provider.of<TaskNotifier>(context, listen: false);
      setState(() {
        var plan = provider.plan();
        this.currentPlan = plan;
      });
    });
  }
  /// Creates a [BarChart] with sample data and no transition
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Center(
          child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
          Container(
            height: 350.0,
            color: new Color(0xFFffe6e6),
            child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              CircleAvatar(
                backgroundImage: NetworkImage(
                  imageUrl,
                ),
                radius: 45,
                backgroundColor: Colors.transparent,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: [
                Text(
                  name,
                  style: TextStyle(
                      fontSize: 25,
                      color: Colors.black38,
                      fontWeight: FontWeight.bold),
                ),
                SizedBox(height: 10), 
                Text(
                  email,
                  style: TextStyle(
                      fontSize: 13,
                      color: Colors.black26,
                      fontWeight: FontWeight.bold),
                )
              ]),
              OutlineButton(
                splashColor: Colors.grey,
                onPressed: () {
                            signOutGoogle();
                            Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) {return LoginPage();}), ModalRoute.withName('/'));
                          },
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(40)),
                highlightElevation: 0,
                borderSide: BorderSide(color: Colors.grey),
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Image(image: AssetImage("assets/google_logo.png"), height: 20.0),
                      Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: Text(
                          'Sign out',
                          style: TextStyle(
                            fontSize: 20,
                            color: Colors.grey,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),

              ])
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                  'Current plan: ${currentPlan == null ? 'Basic' : currentPlan.value}',
                  style: TextStyle(
                      fontSize: 25,
                      color: Colors.black54,
                      fontWeight: FontWeight.bold),
                ),
                ],
              ),
              SizedBox(height: 50), 
              Text(
                  'Your effectivnes in eco areas:',
                  style: TextStyle(
                      fontSize: 16,
                      color: Colors.black45,
                      fontWeight: FontWeight.bold),
                ),
              Card(
                margin: new EdgeInsets.only(left: 40.0, right: 40.0, top: 30.0),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
                elevation: 1.0,
                child: SizedBox(
                  width: 300.0,
                  height: 250.0,
                  child: charts.BarChart(
                    seriesList,
                    animate: animate,
                    vertical: false,
                    barRendererDecorator: new charts.BarLabelDecorator<String>(),
                    // Hide domain axis.
                    domainAxis:
                        new charts.OrdinalAxisSpec(renderSpec: new charts.NoneRenderSpec()),
                    
                )),
              )
          ]),
        ),
    ));
  }

  static List<charts.Series<OrdinalSales, String>> _createSampleData() {
    final data = [
      new OrdinalSales('Waste sorting', 85),
      new OrdinalSales('Renewable materials usage', 60),
      new OrdinalSales('Green energy', 65),
      new OrdinalSales('Not wasting food', 75),
    ];

    return [
      new charts.Series<OrdinalSales, String>(
          id: 'Sales',
          domainFn: (OrdinalSales sales, _) => sales.year,
          measureFn: (OrdinalSales sales, _) => sales.sales,
          data: data,
          colorFn: (_, __) => charts.MaterialPalette.green.shadeDefault,
          // Set a label accessor to control the text of the bar label.
          labelAccessorFn: (OrdinalSales sales, _) =>
              '${sales.year}: ${sales.sales.toString()}%')
    ];
  }
}

class OrdinalSales {
  final String year;
  final int sales;

  OrdinalSales(this.year, this.sales);
}