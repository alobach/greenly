import 'package:flutter/material.dart';
import '../models/colors.dart' as colorModel;
// pages import
import './profilePage.dart';
import './homePage.dart';
import './plansPage.dart';

class LandingPage extends StatefulWidget {
  @override
  LandingPageState createState() => new LandingPageState();
}

class LandingPageState extends State<LandingPage> {
  PageController _pageController;
  int currentIndex = 1;
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30.0, fontWeight: FontWeight.bold);
  static const List<Widget> _widgetOptions = <Widget>[
    PlansPage(),
    HomePage(),
    ProfilePage()
  ];

  @override
  void initState() {
    super.initState();
    _pageController = PageController();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  // @override
  // Widget build(BuildContext context) {
  //   return Scaffold(
  //         body: new SafeArea(
  //             child: new Center(
  //           child: HomePage(),
  //         )),
  //         // bottomNavigationBar: custonBottomBar()),
  //   );
  // }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
          body: new SafeArea(
              child: new Center(
            child: _widgetOptions.elementAt(currentIndex),
          )),
          bottomNavigationBar: custonBottomBar()),
    );
  }

  Widget custonBottomBar() {
    return BottomNavigationBar(
      selectedItemColor: colorModel.dartGreen,
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.list),
          label: 'Plans',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.home),
          label: 'Home',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.people_outline_rounded),
          label: 'Community',
        ),
      ],
      currentIndex: currentIndex,
      // selectedItemColor: Colors.amber[800],
      onTap: (index) => setState(() {
            currentIndex = index;
          }),
    );
  }

  // Widget bottomBar() {
  //   return BottomNavyBar(
  //     selectedIndex: currentIndex,
  //     showElevation: true,
  //     onItemSelected: (index) => setState(() {
  //           currentIndex = index;
  //         }),
  //     items: [
  //       BottomNavyBarItem(
  //         icon: Icon(Icons.people),
  //         title: Text('LIst'),
  //         activeColor: Colors.red,
  //       ),
  //       BottomNavyBarItem(
  //           icon: Icon(Icons.apps),
  //           title: Text('Home'),
  //           activeColor: Colors.purpleAccent),
  //       BottomNavyBarItem(
  //           icon: Icon(Icons.message),
  //           title: Text('Messages'),
  //           activeColor: Colors.pink),
  //     ],
  //   );
  //}
}
