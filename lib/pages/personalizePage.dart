import 'package:flutter/material.dart';
import '../models/colors.dart' as colorModel;
import './landingPage.dart';
import './plansPage.dart';

class FormPage extends StatefulWidget {
  const FormPage({Key key}) : super(key: key);
  
  @override
  _FormPageState createState() => _FormPageState();
}

enum MeatEater { never, rarely, often } 
enum Shopping {yes, no}

class _FormPageState extends State<FormPage> {
  final _formKey = GlobalKey<FormState>();
  MeatEater _character = MeatEater.rarely;
  Shopping _character2 = Shopping.no;

  Map<String, bool> vechicles = {
    'Bike': true,
    'Car': false,
    'Public transport': false,
    'Plane': false,
    'Other': false
  };

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[ 
          Shortcuts(
          shortcuts: <LogicalKeySet, Intent>{
        
          },
          child: Container(
            child: Form(
              autovalidateMode: AutovalidateMode.always,
              // onChanged: () {
              //   Form.of(primaryFocus.context).save();
              // },
              child: Center(
              child: Wrap(
                children: <Widget>[
                  Text(
                    'Personalize Your plan'.toUpperCase(),
                    style: new TextStyle(color: colorModel.slateGreyColor, fontSize: 24.0, fontWeight: FontWeight.bold)
                  ),
                  SizedBox(height: 100),
                  Text(
                    'How often do you eat meat?',
                    style: new TextStyle(color: colorModel.slateGreyColor, fontSize: 16.0, fontWeight: FontWeight.bold)
                  ),
                  SizedBox(height: 30),
                  Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    ListTile(
                      title: const Text('Never'),
                      leading: Radio(
                        value: MeatEater.never,
                        groupValue: _character,
                        onChanged: (MeatEater value) {
                          setState(() {
                            _character = value;
                          });
                        },
                      ),
                    ),
                    ListTile(
                      title: const Text('Rarely'),
                      leading: Radio(
                        value: MeatEater.rarely,
                        groupValue: _character,
                        onChanged: (MeatEater value) {
                          setState(() {
                            _character = value;
                          });
                        },
                      ),
                    ),
                    ListTile(
                      title: const Text('Often'),
                      leading: Radio(
                        value: MeatEater.often,
                        groupValue: _character,
                        onChanged: (MeatEater value) {
                          setState(() {
                            _character = value;
                          });
                        },
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 30),
                Text(
                    'Do you do your own shopping',
                    style: new TextStyle(color: colorModel.slateGreyColor, fontSize: 16.0, fontWeight: FontWeight.bold)
                  ),
                SizedBox(height: 30),
                  Column(
                  children: <Widget>[
                    ListTile(
                      title: const Text('Yes'),
                      leading: Radio(
                        value: Shopping.yes,
                        groupValue: _character2,
                        onChanged: (Shopping value) {
                          setState(() {
                            _character2 = value;
                          });
                        },
                      ),
                    ),
                    ListTile(
                      title: const Text('No'),
                      leading: Radio(
                        value: Shopping.no,
                        groupValue: _character2,
                        onChanged: (Shopping value) {
                          setState(() {
                            _character2 = value;
                          });
                        },
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 30),
                Text(
                    'What means of transport do you use?',
                    style: new TextStyle(color: colorModel.slateGreyColor, fontSize: 16.0, fontWeight: FontWeight.bold)
                  ),
                SizedBox(height: 30),
                TextField(
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: 'Enter here ...'
                  ),
                ),
                SizedBox(height: 100),
                Center(
                  child: Row (
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                    FlatButton(
                          onPressed: () {

                            Navigator.of(context).push(
                              MaterialPageRoute(
                                builder: (context) {
                                  return LandingPage();
                                },
                              ),
                            );
                          },
                          child: Text(
                            "Submit",
                          ),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                            side: BorderSide(color: Colors.green)
                          ),
                        ),
                        SizedBox(height: 50),
                        FlatButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: Text(
                            "Cancel",
                          ),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                            side: BorderSide(color: Colors.green)
                          ),
                        )
                  ])
                    
                )
              ]
              ),
              ),
            ),
          ),
        ),
      ]),
    );
  }
}