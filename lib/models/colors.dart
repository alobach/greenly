import 'package:flutter/material.dart';

const primaryColor = 0xFF333366;
const backgroundColorHex = '#FAFAFB';

Color backgroundColor = const Color.fromRGBO(250, 250, 251, 1.0);
Color greyColor = const Color.fromRGBO(225, 233, 234, 1.0);
Color blackColor = const Color.fromARGB(255, 0, 0, 0);
Color slateGreyColor = const Color.fromRGBO(109, 120, 150, 1.0);
Color jordyColor = const Color.fromRGBO(125, 159, 242, 1.0);
Color illusionColor = const Color.fromRGBO(247, 148, 180, 1.0);
Color peranoColor = const Color.fromRGBO(178, 195, 235, 1.0);
Color quartzColor = const Color.fromRGBO(217, 216, 228, 1.0);
Color dartGreyColor = const Color.fromRGBO(134, 145, 156, 1.0);
Color dartGreen = const Color.fromRGBO(0, 153, 51, 1);