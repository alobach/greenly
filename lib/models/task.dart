import 'package:flutter/foundation.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import '../sign_in.dart';

class Task {
  int id;
  bool active;
  String title;
  String documentId;
  String desc;
  Task( this.id, this.active, this.title, this.documentId, this.desc);
}

class Points
{
  String documentId;
  int value;
  Points(this.value, this.documentId);
}

class Plan
{
  String documentId;
  String value;
  Plan(this.value, this.documentId);
}

class TaskNotifier extends ChangeNotifier {
  List<Task> _tasks = [];
  Points _points;
  Plan _currentPlan;

  List<Task> tasks() {
    fetchData();
    return this._tasks;
  }

  Points points()  {
    fetchData();
    return this._points;
  }

  Plan plan()  {
    fetchData();
    return this._currentPlan;
  }

  TaskNotifier(){this.fetchData();}
    
  void changeState(num id) {
    var toChange = this._tasks.indexWhere((el) => el.id == id);
    this._tasks[toChange].active = !this._tasks[toChange].active;
    if(this._tasks[toChange].active)
    {
      this._points.value += 10;
    }
    else
    {
      this._points.value -= 10;
    }
    notifyListeners();
  }

  void setPlan(String planValue)
  {
    this._currentPlan.value = planValue;
    notifyListeners();
  }


  void addTasks(Task task) {
    this._tasks.add(task);
    notifyListeners();
  }

  void deleteTask(int id) {
    this._tasks.removeWhere((item) => item.id == id);
    notifyListeners();
  }

  void fetchData() async {
    List<Task> tasksfetched = [];
    Points fetchPoints;
    Plan fetchPlan;
    await FirebaseFirestore.instance
        .collection(userId)
        .get()
        .then((QuerySnapshot query){
          var datapoints = query.docs[0].data();
          fetchPoints = new Points(datapoints["points"], query.docs[0].reference.id);
          fetchPlan = new Plan(datapoints["plan"], query.docs[0].reference.id);
          query.docs.forEach((element) {
            var data = element.data();
            data["tasks"].forEach((item) {
              tasksfetched.add(new Task(item["id"], item["active"], item["title"], element.reference.id,item["description"]));
            });
          });
        });
    if(this._points == null)
    {
      this._points = fetchPoints;
    }

    if(this._currentPlan == null)
    {
      this._currentPlan = fetchPlan;
    }

    if(this._tasks == null || this._tasks.isEmpty)
    {
      this._tasks = tasksfetched;
    }
    notifyListeners();
  }
}