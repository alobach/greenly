import 'package:flutter/foundation.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Plan {
  int id;
  String title;
  String documentId;
  String description;

  Plan( this.id, this.title, this.documentId, this.description);
}

class PlanNotifier extends ChangeNotifier {
  List<Plan> _plans = [];
  
  List<Plan> plans() => this._plans;
  
  PlanNotifier() { fetchData(); }

  void fetchData() async {
    List<Plan> plansFetched = [];
    await FirebaseFirestore.instance
        .collection('plans')
        .get()
        .then((QuerySnapshot query) => {
          query.docs.forEach((element) {
            var data = element.data();
            plansFetched.add(new Plan(data["id"], data["title"], element.reference.id, data["description"]));
          })  
        });
    this._plans = plansFetched;
    notifyListeners();
  }
}