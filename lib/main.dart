import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:too_mad/models/plans.dart';
import './models/task.dart';
import 'package:firebase_core/firebase_core.dart';
import 'models/task.dart';
import 'models/plans.dart';
import 'pages/landingPage.dart';
import 'pages/loginPage.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider<TaskNotifier>(builder: (_) => TaskNotifier()),
        ChangeNotifierProvider<PlanNotifier>(builder: (_) => PlanNotifier()),
      ],
      child: new MyApp()
    )
  );
} 

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
      return MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        debugShowCheckedModeBanner: false,
        home: LoginPage()
      );
  }
}
